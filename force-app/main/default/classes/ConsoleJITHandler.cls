//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
global class ConsoleJITHandler implements Auth.SamlJitHandler {
     private class JitException extends Exception{}
    public static void handleUser(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard, List<String> allSalesforceRoleIds) {
        if(create && attributes.containsKey('Username')) {
            u.Username = attributes.get('Username');
        }
        if(create) {
            if(attributes.containsKey('FederationIdentifier')) {
                u.FederationIdentifier = attributes.get('FederationIdentifier');
            } else {
                u.FederationIdentifier = federationIdentifier;
            }
        }
        if (attributes.containsKey('Email')) {
            u.Email = attributes.get('Email');
        }
        if (attributes.containsKey('FirstName')) {
            u.FirstName = attributes.get('FirstName');
        }
        if (attributes.containsKey('LastName')) {
            u.LastName = attributes.get('LastName');
        }
        String alias = '';
        if(u.FirstName == null) {
            alias = u.LastName;
        } else {
            alias = u.FirstName.charAt(0) + u.LastName;
        }
        if(alias.length() > 5) {
            alias = alias.substring(0, 5);
        }
        u.Alias = alias;
        if (attributes.containsKey('MobilePhone')) {
            u.MobilePhone = attributes.get('MobilePhone');
        }
        if (attributes.containsKey('EmployeeNumber')) {
            u.EmployeeNumber = attributes.get('EmployeeNumber');
        }
        if (attributes.containsKey('Department')) {
            u.Department = attributes.get('Department');
        }
        if (hasSObjectField('BE_Number__c', u) && attributes.containsKey('BeNumber')) {
            u.put('BE_Number__c', attributes.get('BeNumber'));
        }
        if (hasSObjectField('NPN__c', u) && attributes.containsKey('NPN')) {
            u.put('NPN__c', attributes.get('NPN'));
        }
        String uid = UserInfo.getUserId();
        User currentUser = [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid];
        u.LocaleSidKey = currentUser.LocaleSidKey;
        u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
        u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
        u.EmailEncodingKey = currentUser.EmailEncodingKey;
        if(attributes.containsKey('Profile')) {
            String profileName = attributes.get('Profile');
            Profile p = [SELECT Id FROM Profile WHERE Name=:profileName];
            if (p == null) {
                throw new JitException('The profile ' + profileName +' not found');
            }
            u.ProfileId = p.Id;
        } else {
            throw new JitException('Profile is required');
        }
       if (attributes.containsKey('RoleId')) {
            String userRole = attributes.get('RoleId');
            if(String.isNotBlank(userRole)){
                UserRole r = [SELECT Id FROM UserRole WHERE Id=:userRole];
                if (r == null) {
                    throw new JitException('The UserRole' + userRole +' not found');
                }
                u.UserRoleId = r.Id;
            } else if (allSalesforceRoleIds.size() > 0 && allSalesforceRoleIds.contains(u.UserRoleId)) {
                u.UserRoleId = null;
            }
        } else if (allSalesforceRoleIds.size() > 0 && allSalesforceRoleIds.contains(u.UserRoleId)) {
                u.UserRoleId = null;
        }
        u.UserPermissionsMarketingUser = attributes.containsKey('UserPermissionsMarketingUser') ? Boolean.valueOf(attributes.get('UserPermissionsMarketingUser')) : false;
        
        u.IsActive = true;
        if(!create) {
            update(u);
        } else {
            insert(u);
        }
    }
    public static boolean hasSObjectField(String fieldName, SObject so){
        return so.getSobjectType().getDescribe().fields.getMap().keySet().contains(fieldName.toLowerCase());
    }
    public static List<String> getSAML(String xmlString, String attributeValue) {
        List<String> listOfAssertionValues = new List<String>();
        String ns = 'urn:oasis:names:tc:SAML:2.0:assertion';
        Dom.document doc = new Dom.Document();    
        doc.load(xmlString);
        dom.XmlNode xroot = doc.getrootelement();
        List<dom.XmlNode> xnAttributeList = xroot.getChildElement('Assertion', ns).getChildElement('AttributeStatement',ns).getChildElements();        
        List<DOM.XmlNode> xnAttributeValList = new List<DOM.XmlNode>();
        for(Dom.XmlNode xn : xnAttributeList){
            //Check Name attibute matches the one you want
            System.debug('xn: ' + xn);
            if(xn.getAttribute('Name','') == attributeValue){
                xnAttributeValList = xn.getChildElements();
                for(Dom.XmlNode xnv : xnAttributeValList){
                    listOfAssertionValues.addAll(xnv.getText().split(','));
                }
            }
        }
        return listOfAssertionValues;    
    }
    public static void addPermissionSet(List<String> permissions,List<String> allPermissions, User u){    
        List<PermissionSet> psIds = [SELECT Id FROM PermissionSet WHERE Name IN :allPermissions AND IsOwnedByProfile = false];
        List<PermissionSetAssignment> psaList = [SELECT Id FROM PermissionSetAssignment 
        WHERE PermissionSetId IN :psIds AND AssigneeId=:u.Id ];
        if(psaList.size() > 0){
            delete(psaList);
        }
        
        for(String permission: permissions){
            PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name=:permission];
            PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = ps.Id, AssigneeId = u.Id);
            insert(psa);                
        }
                
    }
    public static void addGroupSet(List<String> groups, List<String> allGroups, User u){
        List<Group> gIds = [SELECT Id FROM Group WHERE Name IN :allGroups];
        List<GroupMember> grpList= [
        select Id
        from GroupMember
        WHERE GroupId IN :gIds AND UserOrGroupId=:u.Id
        ];
        if(grpList.size() > 0){
            delete(grpList);
        }
        for(String ug: groups){
            Group grp = [SELECT Id FROM Group WHERE Name=:ug];
            GroupMember gm = new GroupMember (GroupId = grp.Id, UserOrGroupId = u.Id);
            insert(gm); 
        }
    }
    
    public static void addPermissionSetGroup(List<String>permissionSetGroups ,List<String> allPermissionSetGroups ,User u){
        List<PermissionSetGroup> psgIds = [SELECT Id FROM PermissionSetGroup WHERE DeveloperName IN :allPermissionSetGroups];
        List<PermissionSetAssignment> psaList = [SELECT Id FROM PermissionSetAssignment 
        WHERE PermissionSetGroupId IN :psgIds AND AssigneeId=:u.Id ];
        if(psaList.size() > 0){
            delete(psaList);
        }
        
        for(String permissionSetGroup: permissionSetGroups){
            PermissionSetGroup psg = [SELECT Id FROM PermissionSetGroup WHERE DeveloperName=:permissionSetGroup];
            PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetGroupId = psg.Id, AssigneeId = u.Id);
            insert(psa);                
        }
    }
    public void handleJit(boolean create, User u, Id samlSsoProviderId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {  
        Blob blobXML = EncodingUtil.base64Decode(assertion);
        List<String> permissionSets = getSAML(blobXML.toString(), 'Permissions');
        List<String> publicGroups = getSAML(blobXML.toString(), 'PublicGroups');
        List<String> permissionSetGroups = getSAML(blobXML.toString(), 'PermissionSetGroups');
        List<String> allPermissionSets = getSAML(blobXML.toString(), 'AllPermissions');
        List<String> allPublicGroups = getSAML(blobXML.toString(), 'AllPublicGroups');
        List<String> allPermissionSetGroups = getSAML(blobXML.toString(), 'AllPermissionSetGroups');
        List<String> allSalesforceRoleIds = getSAML(blobXML.toString(), 'AllSalesforceRoleIds');
        handleUser(create, u, attributes, federationIdentifier, true, allSalesforceRoleIds );
        User userObject = [SELECT Id FROM User WHERE FederationIdentifier=:federationIdentifier];
        addPermissionSet(permissionSets, allPermissionSets, u);
        addGroupSet(publicGroups, allPublicGroups, u);
        addPermissionSetGroup(permissionSetGroups , allPermissionSetGroups , u);
    }
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        User u = new User();
        handleJit(true, u, samlSsoProviderId, 
            federationIdentifier, attributes, assertion);
        return u;
    }
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        User u = [SELECT Id, FirstName, UserRoleId FROM User WHERE FederationIdentifier=:federationIdentifier];
        handleJit(false, u, samlSsoProviderId, 
            federationIdentifier, attributes, assertion);
    }
}